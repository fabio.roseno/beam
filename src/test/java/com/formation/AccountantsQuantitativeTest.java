package com.formation;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.formation.configs.CustomOptions;

public class AccountantsQuantitativeTest {

	@Rule
	public transient ExpectedException thrown = ExpectedException.none();
	
	private static final String BUCKET = "gs://some-bucket/";
	private static final String INPUT_FILE = BUCKET+"some-file";
	private static final String PROJECT = "some-project";
	private static final String INSTANCE = "some-instance";
	private static final String DATASET = "some-dataset";
	private static final String SOURCE_TABLE = "some-src-table";
	private static final String TARGET_TABLE = "some-tgt-table";

	@Test
	public void testNoParamsException() {
		CustomOptions options = PipelineOptionsFactory.as(CustomOptions.class);
		thrown.expectMessage("filepattern can not be null");
		thrown.expect(RuntimeException.class);
//		try {
//			p.run();
//		} catch (Exception e) {
//			assertEquals("Cannot create output file at", e.getMessage());
//		}
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	@Test
	public void testNoProjectException() {
		CustomOptions options = PipelineOptionsFactory.as(CustomOptions.class);
		options.setInputFile(INPUT_FILE);
		thrown.expectMessage("A projectId must be set to configure Bigtable properly.");
		thrown.expect(RuntimeException.class);
		
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	@Test
	public void testNoInstanceException() {
		CustomOptions options = PipelineOptionsFactory.as(CustomOptions.class);
		options.setInputFile(INPUT_FILE);
		options.setProject(PROJECT);
		thrown.expectMessage("A instanceId must be set to configure Bigtable properly.");
		thrown.expect(RuntimeException.class);
		
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	@Test
	public void testNoBigTableException() {
		CustomOptions options = PipelineOptionsFactory.as(CustomOptions.class);
		options.setInputFile(INPUT_FILE);
		options.setProject(PROJECT);
		options.setInstance(INSTANCE);
		thrown.expectMessage("A tableid must be set to configure Bigtable properly.");
		thrown.expect(RuntimeException.class);
		
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	@Test
	public void testNoBTDatasetException() {
		CustomOptions options = PipelineOptionsFactory.as(CustomOptions.class);
		options.setInputFile(INPUT_FILE);
		options.setProject(PROJECT);
		options.setInstance(INSTANCE);
		options.setSourceTable(SOURCE_TABLE);
		thrown.expectMessage("BigQuery dataset not found for table");
		thrown.expect(RuntimeException.class);
		
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	@Test
	public void testNoTgtTableException() {
		CustomOptions options = PipelineOptionsFactory.as(CustomOptions.class);
		options.setInputFile(INPUT_FILE);
		options.setProject(PROJECT);
		options.setInstance(INSTANCE);
		options.setSourceTable(SOURCE_TABLE);
		options.setDataset(DATASET);
		thrown.expectMessage("BigQuery dataset not found for table");
		thrown.expect(RuntimeException.class);
		
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	@Test
	public void testBQNotExistsException() {
		CustomOptions options = PipelineOptionsFactory.as(CustomOptions.class);
		options.setInputFile(INPUT_FILE);
		options.setProject(PROJECT);
		options.setInstance(INSTANCE);
		options.setSourceTable(SOURCE_TABLE);
		options.setDataset(DATASET);
		options.setTargetTable(TARGET_TABLE);
		thrown.expectMessage("BigQuery dataset not found for table");
		thrown.expect(RuntimeException.class);
		
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	static CustomOptions getOptions() {

		final String INPUT_FILE = "gs://ewx-acn-df-temp/loadcurves/scheduled/processed/20210630/lpRegisters_20210629040000_776308.csv";
		final String PROJECT = "ewx-acn";
		final String INSTANCE = "veeinstance-hdd";
		final String DATASET = "accenture_teste";
		final String SOURCE_TABLE = "test_read_beam";
		final String TARGET_TABLE = "quantifiers";
		
		CustomOptions options = PipelineOptionsFactory.as(CustomOptions.class);
		options.setRunner(DataflowRunner.class);
		options.setInputFile(INPUT_FILE);
		options.setProject(PROJECT);
		options.setInstance(INSTANCE);
		options.setSourceTable(SOURCE_TABLE);
		options.setDataset(DATASET);
		options.setTargetTable(TARGET_TABLE);
		return options;
	}
	
	@Test
	public void testRegionNotExistsException() {
		
		CustomOptions options = getOptions();
		thrown.expectMessage("Missing required pipeline options: region");
		thrown.expect(RuntimeException.class);
		
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	@Test
	public void testGcpTempLocationNotExistsException() {

		final String REGION = "europe-west1";
		CustomOptions options = getOptions();
		options.setRegion(REGION);
		thrown.expectMessage("DataflowRunner requires gcpTempLocation, but failed to retrieve a value from PipelineOptions");
		thrown.expect(RuntimeException.class);
		
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	@Test
	public void testTempLocationNotExistsException() {
		
		final String REGION = "europe-west1";
		final String BUCKET = "gs://ewx-acn-df-temp/dataflow/temp_fabio/beam/";
		CustomOptions options = getOptions();
		options.setRegion(REGION);
		options.setGcpTempLocation(BUCKET);
		thrown.expectMessage("BigQueryIO.Write needs a GCS temp location to store temp files.");
		thrown.expect(RuntimeException.class);
		
		AccountantsQuantitative.runDataGrouping(options);
	}
	
	@Test
	public void testRunDataflow() {
		
		final String REGION = "europe-west1";
		final String BUCKET = "gs://ewx-acn-df-temp/dataflow/temp_fabio/beam/";
		CustomOptions options = getOptions();
		options.setRegion(REGION);
		options.setGcpTempLocation(BUCKET);
		options.setTempLocation(BUCKET);
		
		AccountantsQuantitative.runDataGrouping(options);
	}

}
