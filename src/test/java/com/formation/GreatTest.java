package com.formation;

import static org.junit.Assert.assertThat;

import java.net.URL;

import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.testing.FileChecksumMatcher;
import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.testing.TestPipelineOptions;
import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.util.NumberedShardedFile;
import org.apache.beam.sdk.values.PCollection;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.formation.configs.GreatOptions;
import com.google.common.io.Resources;

/**
 * Unit test for Great Pipeline
 */
public class GreatTest {

	@Rule
	public TestPipeline pipe = TestPipeline.create();
	
	@Rule
	public transient ExpectedException thrown = ExpectedException.none();

	final URL FILE = Resources.getResource("text.txt");
	
	final GreatOptions options = TestPipeline.testingPipelineOptions().as(GreatOptions.class);

	@BeforeClass
	public static void setup() {
		PipelineOptionsFactory.register(TestPipelineOptions.class);
	}

	@Test
	public void testGcpTempLocationInvalid() throws Exception {

		options.setTempLocation("gs://some-bucket/");

		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Error constructing default value for gcpTempLocation: " 
						   + "tempLocation is not a valid GCS path");
		options.getGcpTempLocation();
	}

	@Test
	public void testNonexistentInputFile() throws Exception {

		thrown.expect(RuntimeException.class);
		thrown.expectMessage("filepattern can not be null");
		Great.runPipeline(options);
	}

	@Test
	public void testCountLinesInputFile() {
		PCollection<String> inputFile = pipe.apply("Read File", TextIO.read().from(FILE.getPath()));
		PCollection<Long> inputCount = inputFile.apply(Count.<String>globally());
		PAssert.that(inputCount).containsInAnyOrder(13L);
		pipe.run();
	}

//	@Test
	public void testUppercase() {

		options.setInputFile(FILE.getPath());
		options.setOutputFile("C:\\dev\\projects\\temp\\mobie\\output");

		Great.runPipeline(options);
		assertThat(new NumberedShardedFile(options.getOutputFile() + "*"),
				FileChecksumMatcher.fileContentsHaveChecksum("7923b7a1b9f700f31119b85eed27e019b0b6e04e"));

	}

}
