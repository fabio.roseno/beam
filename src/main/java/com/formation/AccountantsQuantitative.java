package com.formation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.StreamSupport;

import com.formation.configs.CustomOptions;
import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.cloud.bigtable.beam.CloudBigtableIO;
import com.google.cloud.bigtable.beam.CloudBigtableScanConfiguration;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.Read;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.transforms.WithKeys;
import org.apache.beam.sdk.transforms.join.CoGbkResult;
import org.apache.beam.sdk.transforms.join.CoGroupByKey;
import org.apache.beam.sdk.transforms.join.KeyedPCollectionTuple;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TupleTag;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Import CSV data and BigTable data to sum counters to save in BigQuery
 * 
 * Configure debug dataflow launch on VSCode:
 * 
 * {
 *   "type": "java",
 *   "name": "Launch AccountantsQuantitative",
 *   "request": "launch",
 *   "mainClass": "com.formation.AccountantsQuantitative",
 *   "projectName": "beam",
 *   "args": ["--runner=DataflowRunner",  //"--runner=direct"
 *            "--workerMachineType=n1-standard-1",
 *            "--maxNumWorkers=5",
 *            "--project=[ PROJECT ]",
 *            "--region=[ REGION ]",
 *            "--inputFile=gs://[ INPUT_FILE ]",
 *            "--dataset=[ DATASET ]",
 *            "--instance=[ INSTANCE ]",
 *            "--sourceTable=[ SOURCE_TABLE ]",
 *            "--targetTable=[ TARGET_TABLE ]",
 *            "--gcpTempLocation=gs://[ GCP_TEMP_LOCATION ]",
 *            "--tempLocation=gs://[ TEMP_LOCATION ]"]
 * }
 * 
 * Or run on PowerShell:
 * 
 * mvn compile exec:java `
 *     -D exec.mainClass=com.formation.AccountantsQuantitative `
 *     -D exec.args="--runner=DataflowRunner `
 *                   --workerMachineType=n1-standard-1 `
 *                   --maxNumWorkers=5 `
 *                   --project=[ PROJECT ] `
 *                   --region=[ REGION ] `
 *                   --inputFile=gs://[ INPUT_FILE ] `
 *                   --dataset=[ DATASET ] `
 *                   --instance=[ INSTANCE ] `
 *                   --sourceTable=[ SOURCE_TABLE ] `
 *                   --targetTable=[ TARGET_TABLE ] `
 *                   --gcpTempLocation=gs://[ GCP_TEMP_LOCATION ] `
 *                   --tempLocation=gs://[ TEMP_LOCATION ]"
 */
public class AccountantsQuantitative {
    
    private static final Logger LOG = LoggerFactory.getLogger(AccountantsQuantitative.class);

    static TableSchema getSchema() {
        return new TableSchema()
                .setFields(
                    Arrays.asList(
                        new TableFieldSchema()
                            .setName("str_date")
                            .setType("STRING")
                            .setMode("NULLABLE"),
                        new TableFieldSchema()
                            .setName("counter_id")
                            .setType("STRING")
                            .setMode("NULLABLE"),
                        new TableFieldSchema()
                            .setName("hashed_counter_id")
                            .setType("STRING")
                            .setMode("NULLABLE"),
                        new TableFieldSchema()
                            .setName("offset_qty")
                            .setType("INT64")
                            .setMode("NULLABLE"),
                        new TableFieldSchema()
                            .setName("lines_bt_qty")
                            .setType("INT64")
                            .setMode("NULLABLE")));
    }

    static class PrepareTableRowFn extends DoFn<KV<String, CoGbkResult>, TableRow> {

        private TupleTag<String> tagInputFile;
        private TupleTag<Result> tagInputBigtable;

        public PrepareTableRowFn(TupleTag<String> tagInputFile, TupleTag<Result> tagInputBigtable) {
            this.tagInputFile = tagInputFile;
            this.tagInputBigtable = tagInputBigtable;
        }

        @ProcessElement
        public void processElement(ProcessContext element) {

            KV<String, CoGbkResult> input = element.element();
            CoGbkResult join = input.getValue();

            Integer offsetQty = 0;
            var counterId = "";
            try {

                Iterable<String> listOfInputFile = join.getAll(tagInputFile);
                Set<String> offsetList = new HashSet<String>();
                for (String line : listOfInputFile) {
                    String[] values = line.split(";");
                    counterId = values[0];
                    for (var i = 4; i < values.length; i++) {
                        String[] offsets = values[i].split("[|]+");
                        offsetList.add(offsets[0]);
                    }                    
                }

                offsetQty = offsetList.size();

            } catch (Exception e) {
                LOG.error("There isn't data on file entry with key " + input.getKey(), e);
            }
            
            try {
                join.getOnly(tagInputBigtable);
            } catch (Exception e) {
                LOG.error("There isn't data on bigtable entry with key " + input.getKey(), e);
            }
            
            Iterable<Result> listOfInputBigtable = join.getAll(tagInputBigtable);
            long linesBtQty = StreamSupport.stream(listOfInputBigtable.spliterator(), false).count();

            String[] arrKey = input.getKey().split("#");
            String strDate = arrKey[1];
            String hashedCounterId = arrKey[0];

            TableRow row = new TableRow().set("str_date", strDate)
                                         .set("counter_id", counterId)
                                         .set("hashed_counter_id", hashedCounterId)
                                         .set("offset_qty", offsetQty)
                                         .set("lines_bt_qty", linesBtQty);
            element.output(row);
        }
    }

    static void runDataGrouping(CustomOptions options) {

    	final var INPUT_FILE = options.getInputFile();
    	final var PROJECT = options.getProject();
    	final var DATASET = options.getDataset();
    	final var INSTANCE = options.getInstance();
    	final var SOURCE_TABLE = options.getSourceTable();
    	final var TARGET_TABLE = options.getTargetTable();

    	var pipeline = Pipeline.create(options);
        
        CloudBigtableScanConfiguration configBT = new CloudBigtableScanConfiguration.Builder()
                                                                                    .withProjectId(PROJECT)
                                                                                    .withInstanceId(INSTANCE)
                                                                                    .withTableId(SOURCE_TABLE)
                                                                                    .build();

        PCollection<String> inputFile = pipeline.apply("Read CSV File", TextIO.read().from(INPUT_FILE));
        PCollection<KV<String, String>> inputFileKey = inputFile.apply("Add Input File Keys",
                WithKeys.of(new SerializableFunction<String, String>() {
                    public String apply(String s) {
                        String[] values = s.split(";");
                        return DigestUtils.md5Hex(values[0]) + "#" + values[1].substring(0, 10);
                    }
                }));

        PCollection<Result> inputBigtable = pipeline.apply("Read Bigtable", Read.from(CloudBigtableIO.read(configBT)));
        PCollection<KV<String, Result>> inputBigtableKey = inputBigtable.apply("Add Input Bigtable Keys",
                WithKeys.of(new SerializableFunction<Result, String>() {
                    public String apply(Result result) {
                        return Bytes.toString(result.getRow()).substring(0, 43);
                    }
                }));
        
        final TupleTag<String> tagInputFile = new TupleTag<>();
        final TupleTag<Result> tagInputBigtable = new TupleTag<>();

        KeyedPCollectionTuple<String> pTuppe = KeyedPCollectionTuple.of(tagInputFile, inputFileKey).and(tagInputBigtable, inputBigtableKey);
        PCollection<KV<String, CoGbkResult>> result = pTuppe.apply("CoGBK", CoGroupByKey.create());

        result.apply("Preparing TableRow", ParDo.of(new PrepareTableRowFn(tagInputFile, tagInputBigtable)))
              .apply("Write to BigQuery", BigQueryIO.writeTableRows()
                                                    .to(String.format("%s:%s.%s", PROJECT, DATASET, TARGET_TABLE))
                                                    .withSchema(getSchema())
                                                    .withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED)
                                                    .withWriteDisposition(WriteDisposition.WRITE_APPEND));

        pipeline.run().waitUntilFinish();

    }
    
    public static void main(String[] args) {

        CustomOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(CustomOptions.class);
        runDataGrouping(options);

    }

}
