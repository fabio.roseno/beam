package com.formation.transf;

import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.values.TypeDescriptors;

public class Transforms
{

    public static MapElements<String, String> mapToUpper()
    {
    	return MapElements.into(TypeDescriptors.strings()).via((String obj) -> obj.toUpperCase());
	}
    
}
