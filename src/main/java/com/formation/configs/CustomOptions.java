package com.formation.configs;

import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.options.Description;

public interface CustomOptions extends DataflowPipelineOptions {

    @Description("Input File Path")
    String getInputFile();
    void setInputFile(String inputFile);

    @Description("Instance name")
    String getInstance();
    void setInstance(String instance);

    @Description("Dataset name")
    String getDataset();
    void setDataset(String dataset);
    
    @Description("Source table name")
    String getSourceTable();
    void setSourceTable(String sourceTable);
    
    @Description("Target table name")
    String getTargetTable();
    void setTargetTable(String targetTable);
    
    @Description("Query fields")
    String getQueryFields();
    void setQueryFields(String queryFields);
    
    @Description("Query filter")
    String getQueryFilter();
    void setQueryFilter(String queryFilter);
    
}
