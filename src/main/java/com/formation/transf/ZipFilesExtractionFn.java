package com.formation.transf;

import java.io.ByteArrayOutputStream;
import java.nio.channels.Channels;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.beam.sdk.io.FileIO.ReadableFile;
import org.apache.beam.sdk.io.FileSystems;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZipFilesExtractionFn extends DoFn<ReadableFile, KV<String, String[]>>{

	private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(ZipFilesExtractionFn.class);

	@ProcessElement
    public void processElement(ProcessContext c) {
		try (ZipInputStream zip = new ZipInputStream(
				Channels.newInputStream(FileSystems.open(c.element().getMetadata().resourceId())))) {

			ZipEntry entry = null;

			while ((entry = zip.getNextEntry()) != null) {
				byte[] buffer = new byte[1024];
				ByteArrayOutputStream builder = new ByteArrayOutputStream();
				int end;
				while ((end = zip.read(buffer)) > 0) {
					builder.write(buffer, 0, end);
				}

				KV<String, String[]> kv = KV.of(entry.getName(),
						new String(builder.toByteArray(), StandardCharsets.UTF_8).split("\\r?\\n"));

				LOG.info("File name: {}", kv.getKey());
				c.output(kv);
			}

		} catch (Exception e) {
			LOG.error("Read from zip exception", e);
		}
    	
    }
    
}
