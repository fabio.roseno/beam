package com.formation;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.testing.ValidatesRunner;
import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PInput;
import org.apache.beam.sdk.values.POutput;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.bigtable.data.v2.BigtableDataClient;
import com.google.cloud.bigtable.data.v2.models.RowCell;

/**
 * Unit test for WordCountSumTest.
 */
public class WordCountSumTest 
{

    private static final Logger LOG = LoggerFactory.getLogger(WordCountSumTest.class);
    
    @Rule
    public final TestPipeline tp = TestPipeline.create();
    
    private static final String[] WORDS_ARRAY = new String[] {
    	    "hi", "there", "hi", "hi", "sue", "bob",
    	    "hi", "sue", "", "", "ZOW", "bob", ""};

    private static final List<String> WORDS = Arrays.asList(WORDS_ARRAY);
    
    @Test
    public void wordCountTest() {
  
      // Create an input PCollection.
      PCollection<String> input = tp.apply(Create.of(WORDS));
  
      // Apply the Count transform under test.
      PCollection<KV<String, Long>> output =
        input.apply(Count.<String>perElement());
  
      // Assert on the results.
      PAssert.that(output)
        .containsInAnyOrder(
            KV.of("hi", 4L),
            KV.of("there", 1L),
            KV.of("sue", 2L),
            KV.of("bob", 2L),
            KV.of("", 3L),
            KV.of("ZOW", 1L));
  
      // Run the pipeline.
      tp.run();
    }
    
    @Test
    @Category(ValidatesRunner.class)
    public void identityTransformTest() {
  
      PCollection<Integer> output = tp.apply(Create.of(1, 2, 3, 4)).apply("IdentityTransform", new IdentityTransform<>());  
      PAssert.that(output).containsInAnyOrder(1, 2, 3, 4);
      
      tp.run();
    }

    private static class IdentityTransform<T extends PInput & POutput> extends PTransform<T, T> {
        @Override
        public T expand(T input) {
            return input;
        }
    }
    
    @Test
    public void runSplitPipe() {
        String str = "105|0|00";
        String[] arrOfStr = str.split("[|]+");
        for (String a : arrOfStr) {
        	LOG.info(a);
        }
    }
    
    @Test
    public void readBigTableRow() {

    	String projectId = "ewx-acn";
    	String instanceId = "veeinstance-hdd";
    	String tableId = "test_read_beam";
    	String rowkey = "2e6b5a8fb8871da6ed7f94daa8df7889#2021-06-28T22:30:00+00:00";
    	
        try (BigtableDataClient dataClient = BigtableDataClient.create(projectId, instanceId)) {

            var row = dataClient.readRow(tableId, rowkey);
            if(row != null) {
            	String colFamily = "";
                for (RowCell cell : row.getCells()) {
                    if (!cell.getFamily().equals(colFamily)) {
                        colFamily = cell.getFamily();
                        System.out.printf("Column Family %s%n", colFamily);
                    }
                    System.out.printf("\t%s: %s @%s%n", cell.getQualifier().toStringUtf8(), cell.getValue().toStringUtf8(), cell.getTimestamp());
                }
            } else {
                LOG.error("Register dont exist!");
            }

        } catch (IOException e) {
            LOG.error("Unable to initialize service client, as a network error occurred.", e);
        }
    }
    
    @Test
    public void encryptMD5() {
    	String counter = "1010000001716214007";
    	LOG.info(DigestUtils.md5Hex(counter));
    }
      
}
