package com.formation;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.Compression;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.formation.configs.CustomOptions;
import com.formation.transf.ZipFilesExtractionFn;

public class Preprocessing {

    private static final Logger LOG = LoggerFactory.getLogger(Preprocessing.class);
	
	public static void main(String[] args) {
		CustomOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(CustomOptions.class);
		Pipeline pipe = Pipeline.create(options);

		// Get All Sgls From ZipFile
		PCollection<KV<String, String[]>> inputSgls = pipe
				.apply("Read InputZipFile", FileIO.match().filepattern(options.getInputFile()))
				.apply("Set Zip Compression", FileIO.readMatches().withCompression(Compression.ZIP))
				.apply("Read ZipFile", ParDo.of(new ZipFilesExtractionFn()));
//
//		// Prepare Write CSV in BQ
//		PCollection<TableRow> mappingBQ = inputSgls.apply("Mapping To BQ",
//				MapElements.via(DataExtractions.mappingToBQ()));
		
		
		pipe.run().waitUntilFinish();
		
	}

}
