package com.formation;

import com.formation.configs.CustomOptions;
import com.google.api.services.bigquery.model.TableRow;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;

/**
 * Read the word sum query to save in BigQuery
 * 
 * Configure debug dataflow launch on VSCode:
 * 
 * {
 *   "type": "java",
 *   "name": "Launch WordCountSum",
 *   "request": "launch",
 *   "mainClass": "com.formation.WordCountSum",
 *   "projectName": "beam",
 *   "args": ["--runner=DataflowRunner",  //"--runner=direct"
 *            "--workerMachineType=n1-standard-1",
 *            "--maxNumWorkers=5",
 *            "--project=[ PROJECT ]",
 *            "--region=[ REGION ]",
 *            "--dataset=[ DATASET ]",
 *            "--sourceTable=[ SOURCE_TABLE ]",
 *            "--targetTable=[ TARGET_TABLE ]",
 *            "--gcpTempLocation=gs://[ GCP_TEMP_LOCATION ]",
 *            "--tempLocation=gs://[ TEMP_LOCATION ]"]
 * }
 * 
 * Or run on PowerShell:
 * 
 * mvn compile exec:java `
 *     -D exec.mainClass=com.formation.WordCountSum `
 *     -D exec.args="--runner=DataflowRunner `
 *                   --workerMachineType=n1-standard-1 `
 *                   --maxNumWorkers=5 `
 *                   --project=[ PROJECT ] `
 *                   --region=[ REGION ] `
 *                   --dataset=[ DATASET ] `
 *                   --sourceTable=[ SOURCE_TABLE ] `
 *                   --targetTable=[ TARGET_TABLE ] `
 *                   --gcpTempLocation=gs://[ GCP_TEMP_LOCATION ] `
 *                   --tempLocation=gs://[ TEMP_LOCATION ]" `
 *   
 */
public class WordCountSum {

  static class WordsCountBQFn extends DoFn<TableRow, TableRow> {

    @ProcessElement
    public void processElement(@Element TableRow element, OutputReceiver<TableRow> receiver) {
      receiver.output(element);
    }
  }
      
  static void runWordCountBigQuery(CustomOptions options) {
    
    String PROJECT = options.getProject();
    String DATASET = options.getDataset();
    String SOURCE_TABLE = options.getSourceTable();
    String TARGET_TABLE = options.getTargetTable();

    String tableSchemaJson = ""
        + "{"
        + "  \"fields\": ["
        + "    {"
        + "      \"name\": \"title\","
        + "      \"type\": \"STRING\","
        + "      \"mode\": \"NULLABLE\""
        + "    },"
        + "    {"
        + "      \"name\": \"word\","
        + "      \"type\": \"STRING\","
        + "      \"mode\": \"NULLABLE\""
        + "    },"
        + "    {"
        + "      \"name\": \"qty\","
        + "      \"type\": \"INTEGER\","
        + "      \"mode\": \"NULLABLE\""
        + "    }"
        + "  ]"
        + "}";

    String SUMMARIZE_QUERY = "SELECT  title, "
                                    + "word, "
                                    + "SUM(qty) AS qty "
                            + "FROM `%s.%s.%s` "
                            + "GROUP BY title, "
                                     + "word;";

    Pipeline p = Pipeline.create(options);

    PCollection<TableRow> rows = p.apply("Read from BigQuery", BigQueryIO.readTableRows()
                                                              .fromQuery(String.format(SUMMARIZE_QUERY, PROJECT, DATASET, SOURCE_TABLE))
                                                              .withoutValidation()
                                                              .usingStandardSql());

    rows.apply("Transform Data", ParDo.of(new WordsCountBQFn()))
        .apply("Write to BigQuery", BigQueryIO.writeTableRows()
                                    .to(String.format("%s:%s.%s", PROJECT, DATASET, TARGET_TABLE))
                                    .withJsonSchema(tableSchemaJson)
                                    .withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED)
                                    .withWriteDisposition(WriteDisposition.WRITE_TRUNCATE));

    p.run().waitUntilFinish();
  }

  public static void main(String[] args) {

      CustomOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(CustomOptions.class);
      runWordCountBigQuery(options);

  }
}
