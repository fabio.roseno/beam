package com.formation.configs;

import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.options.Description;

public interface GreatOptions extends DataflowPipelineOptions
{

    @Description("Input File Path")
    String getInputFile();
    void setInputFile(String inputFile);

    
    @Description("Output File Path")
    String getOutputFile();
    void setOutputFile(String outputFile);
    
}
