package com.formation;

import static org.junit.Assert.assertEquals;

import java.io.Serializable;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.View;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionView;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.formation.configs.GreatOptions;
import com.google.common.io.Resources;

@RunWith(JUnit4.class)
public class MappingTest implements Serializable {

	private static final long serialVersionUID = 1L;

	@Rule
	public transient TestPipeline pipe = TestPipeline.create();

	final GreatOptions options = TestPipeline.testingPipelineOptions().as(GreatOptions.class);
	
	final URL FILE = Resources.getResource("lowerNames.csv");
	
	final String output = "C:\\temp\\mapping\\upperNames";
	
	@Before
	public void setup() {
		options.setInputFile(FILE.getPath());
		options.setOutputFile(output);
	}
	
//	@Test
    public void testPipelineMapping() {
		Mapping.runPipe(options);		
	}
	
//    @Test
    public void testSideInputAsList() {
    	PCollectionView<List<Integer>> sideInputView = pipe.apply("Create sideInput", Create.of(1, 2, 3))
				.apply(View.asList());
		PCollection<Integer> input = pipe.apply("Create input", Create.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
				.apply(ParDo.of(new DoFn<Integer, Integer>() {
					private static final long serialVersionUID = 7729171579044198808L;

					@ProcessElement
					public void processElement(ProcessContext c) {
						List<Integer> sideInputValue = c.sideInput(sideInputView);
						if (!sideInputValue.contains(c.element())) {
							c.output(c.element());
						}
					}
				}).withSideInputs(sideInputView));
      PAssert.that(input).containsInAnyOrder(4, 5, 6, 7, 8, 9, 10);
      pipe.run();
    }
    
//    @Test
    public void testWriteWithMissingSchemaFromView() throws Exception {
      PCollectionView<Map<String, String>> view =
          pipe.apply("Create schema view", Create.of(KV.of("foo", "bar"), KV.of("bar", "boo")))
              .apply(View.asMap());

		PCollection<Integer> input = pipe.apply("Create input", Create.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
				.apply(ParDo.of(new DoFn<Integer, Integer>() {
					private static final long serialVersionUID = 1L;

					@ProcessElement
					public void processElement(ProcessContext c) {
						Map<String, String> sideInputValue = c.sideInput(view);
					    System.out.println(sideInputValue);
						c.output(c.element());
					}
				}).withSideInputs(view));
      pipe.run();
    }
    
    @Test
    public void testViewGetName() {
      assertEquals("View.AsSingleton", View.<String>asSingleton().getName());
      assertEquals("View.AsIterable", View.<Integer>asIterable().getName());
      assertEquals("View.AsMap", View.<String, Integer>asMap().getName());
      assertEquals("View.AsMultimap", View.<String, Integer>asMultimap().getName());
    }

}
