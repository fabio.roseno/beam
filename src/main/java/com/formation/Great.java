package com.formation;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.fs.EmptyMatchTreatment;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.formation.configs.GreatOptions;

/**
 * Dataflow Pipeline Great
 */
public class Great 
{

	private static final Logger LOG = LoggerFactory.getLogger(Great.class);
	
	static void runPipeline(GreatOptions options)
	{
		LOG.info("Starting Great Pipeline...");
		var pipe = Pipeline.create(options);

		pipe.apply("Read InputFile",
				TextIO.read().from(options.getInputFile()).withEmptyMatchTreatment(EmptyMatchTreatment.ALLOW))
				.apply("UpperCASE", ParDo.of(new DoFn<String, String>() {
					@ProcessElement
					public void processElement(@Element String input, OutputReceiver<String> out) {
						out.output(input.toUpperCase());
					}
				})).apply("Write OutputFile", TextIO.write().to(options.getOutputFile()));

		pipe.run().waitUntilFinish();
		LOG.info("Finished Great Pipeline...");
		
	}

    public static void main( String[] args )
    {
    	GreatOptions options = PipelineOptionsFactory.fromArgs(args)
    												 .withValidation()
    												 .as(GreatOptions.class);
    	runPipeline(options);
    	
    }
}
