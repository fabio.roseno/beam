package com.formation;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.transforms.WithKeys;
import org.apache.beam.sdk.values.TypeDescriptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.formation.configs.CustomOptions;
import com.google.api.services.bigquery.model.TableRow;

public class ReadBQ {

	private static final Logger LOG = LoggerFactory.getLogger(ReadBQ.class);
	private static void runPipeline(CustomOptions options) {

		var pipeline = Pipeline.create(options);

		LOG.info("Starting ReadBT Pipeline...");
		pipeline.apply("Read from BigQuery", BigQueryIO.readTableRows()
												.fromQuery("SELECT "+options.getQueryFields()+" \r\n"
														+ "FROM "+options.getDataset()+"."+options.getSourceTable()+ " \r\n"
														+ "WHERE "+options.getQueryFilter())
												.usingStandardSql()
												.withMethod(BigQueryIO.TypedRead.Method.EXPORT))
				.apply("Extract datasource IDs", MapElements.into(TypeDescriptors.strings())
								.via((TableRow row) -> "POD: " +row.get("pcve")+ " - EQUIP: " +row.get("equipment")))
				.apply("Map Key from BQ", WithKeys.of(new SerializableFunction<String, String>() {
					private static final long serialVersionUID = -3527555454118256759L;

					public String apply(String s) {
						LOG.info(s);
						return s;
					}
				}));

		pipeline.run().waitUntilFinish();
		LOG.info("Finished ReadBT Pipeline...");
	}

	public static void main(String[] args) {
		CustomOptions options = PipelineOptionsFactory
				.fromArgs(args).withValidation().as(CustomOptions.class);
		runPipeline(options);
	}

}
