package com.formation;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.formation.configs.GreatOptions;
import com.formation.transf.Transforms;

public class Mapping 
{

	private static final Logger LOG = LoggerFactory.getLogger(Mapping.class);

    static void runPipe(GreatOptions options) 
    {
    	LOG.info("Starting Mapping Pipeline...");
		var p = Pipeline.create();
		p.apply("Read File", TextIO.read().from(options.getInputFile()))
		 .apply("Mapping", Transforms.mapToUpper())
		 .apply("Write File", TextIO.write().to(options.getOutputFile()).withNumShards(1).withSuffix(".csv"));
		p.run().waitUntilFinish();
		LOG.info("Finished Mapping Pipeline...");

    }

    public static void main( String[] args )
    {
    	GreatOptions options = PipelineOptionsFactory.fromArgs(args)
													 .withValidation()
													 .as(GreatOptions.class);
    	runPipe(options);
    }
    
}
